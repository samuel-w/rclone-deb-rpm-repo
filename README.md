# rclone-deb-rpm-repo

Always up-to-date [rclone](https://github.com/rclone/rclone) repository




## How to install for RPM-based Linux distributions

### Step 1 - Add repository

Add the repository

#### Fedora/RHEL:
```sh
sudo tee -a /etc/yum.repos.d/rclone.repo << 'EOF'
[gitlab.com_paulcarroty_rclone_repo]
name=gitlab.com_paulcarroty_rclone_repo
baseurl=https://paulcarroty.gitlab.io/rclone-deb-rpm-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/rclone-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```

#### openSUSE/SUSE:
```sh
sudo tee -a /etc/zypp/repos.d/rclone.repo << 'EOF'
[gitlab.com_paulcarroty_rclone_repo]
name=gitlab.com_paulcarroty_rclone_repo
baseurl=https://paulcarroty.gitlab.io/rclone-deb-rpm-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/rclone-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF
```
### Step 2 - Install app

Then type `rclone` in [GNOME Software](https://wiki.gnome.org/Apps/Software) or use your package manager:

```sh
dnf install rclone
zypper in rclone
```




## How to install for **Debian/Ubuntu/Linux Mint**


### Option 1 - (Recommended)

- Add my key:

    ```sh
    wget -qO - https://gitlab.com/paulcarroty/rclone-deb-rpm-repo/raw/master/pub.gpg \
        | gpg --dearmor \
        | sudo dd of=/usr/share/keyrings/rclone-archive-keyring.gpg
    ```
 
- Add the repository:

    ```sh
    echo 'deb [ signed-by=/usr/share/keyrings/rclone-archive-keyring.gpg ] https://paulcarroty.gitlab.io/rclone-deb-rpm-repo/debs rclone main' \
        | sudo tee /etc/apt/sources.list.d/rclone.list
    ```

- Update then install rclone:

    ```sh
    sudo apt update
    sudo apt install rclone
    ```

- Then search for `rclone` and run it (e.g. the Activities menu from the Gnome Panel, or whatever else you use as your launcher or application manager).


### Option 2 - (Alternative) use `apt-key`

If `software-properties-common` package is available, you can use `apt-add` and `apt-add-repository` to add the repository and its key.

```sh
wget -qO - https://gitlab.com/paulcarroty/rclone-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
sudo apt-add-repository 'deb [ signed-by=/etc/apt/trusted.gpg.d/rclone-archive-keyring.gpg ] https://paulcarroty.gitlab.io/rclone-deb-rpm-repo/debs/ rclone main'
sudo apt update
sudo apt install rclone
```

## Verification

Checksum verification doesn't work because GPG signature changes the size of packages.
You can use `diff -r` for extracted packages or [pkgdiff](https://github.com/lvc/pkgdiff).
