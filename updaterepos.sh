#!/bin/sh

LATEST=`curl -s https://api.github.com/repos/rclone/rclone/releases/latest | grep -oP '"tag_name": "\K(.*)(?=")'`
echo -e "\e[0;32mDownload v$LATEST RPM packages...\e[0m"
mkdir -p pkgs/rpms

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'linux-386.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o pkgs/rpms/rclone-$LATEST-x86_64.rpm \
  || { echo "Failed to download x86_64 rpm"; exit 1; }


curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'arm64.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-aarch64.rpm

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'arm-v7.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-armv7hl.rpm

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'mips.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-mips.rpm

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'mipsle.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-mipsel.rpm

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'i386.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-i386.rpm

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'arm.rpm' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o pkgs/rpms/rclone-$LATEST-arm.rpm


  
echo -e "\e[0;32mDownload v$LATEST DEB packages...\e[0m"
mkdir pkgs/debs

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'amd64.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o /tmp/rclone-$LATEST-amd64.deb \
  ||  { echo "Failed to download amd64 deb"; exit 1; }

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'linux-386.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl --fail -L -o /tmp/rclone-$LATEST-i386.deb

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'arm64.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o /tmp/rclone-$LATEST-arm64.deb

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'arm-v7.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o /tmp/rclone-$LATEST-armhf.deb

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'mips.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o /tmp/rclone-$LATEST-mips.deb

curl -s https://api.github.com/repos/rclone/rclone/releases/latest \
  | grep browser_download_url \
  | grep 'mipsle.deb' \
  | cut -d '"' -f 4 \
  | xargs -n 1 curl -L -o /tmp/rclone-$LATEST-mipsel.deb

# reprepro configuration
mkdir pkgs/debs/conf
touch pkgs/debs/conf/{option,distributions}
echo 'Codename: rclone' >> pkgs/debs/conf/distributions
echo 'Components: main' >> pkgs/debs/conf/distributions
echo 'Architectures: amd64 i386 arm64 armhf mips mipsel' >> pkgs/debs/conf/distributions
echo 'SignWith: FF3836CF' >> pkgs/debs/conf/distributions


echo -e "\e[0;32mSign the repositories\e[0m"
# extract the public and private GPG keys from encrypted archive keys.tar with 
# the secret openssl pass KEY, which is stored in GitlabCI variables
openssl aes-256-cbc -d -in keys.tar.enc -out keys.tar -pbkdf2 -k $KEY
tar xvf keys.tar

#signing the repository metadata with my personal GPG key
gpg2 --import keys/pub.gpg && gpg2 --import keys/priv.gpg
expect -c "spawn gpg2 --edit-key 608AEE7C9A46BCE15E557B28F0CEC762FF3836CF trust quit; send \"5\ry\r\"; expect eof"
rpm --define "_gpg_name Samuel Woon <samuel.woon@protonmail.com>" --addsign pkgs/rpms/*rpm

# generate and sign RPM repository
# createrepo pkgs/rpms/         # obsoleted tool
createrepo_c --database --compatibility pkgs/rpms/
gpg2 --local-user "Samuel Woon <samuel.woon@protonmail.com>" --yes --detach-sign --armor pkgs/rpms/repodata/repomd.xml # generate new deb repository
reprepro -V -b pkgs/debs includedeb rclone /tmp/*deb


echo -e "\e[0;32mList of imported public and private keys:"
gpg2 --list-keys && gpg2 --list-secret-keys

echo -e "\e[0;32mDeploy to Gitlab Pages...\e[0m"

# DOCS
# https://linux.die.net/man/8/createrepo
# http://manpages.ubuntu.com/manpages/trusty/man1/dpkg-scanpackages.1.html
# https://github.com/circleci/encrypted-files

